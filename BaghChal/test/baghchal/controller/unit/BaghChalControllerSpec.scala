package baghchal.controller.unit

import org.specs2.mutable._
import org.specs2.runner._
import org.junit.runner._
import baghchal.controller.BaghChalController
import scala.swing.Reactor
import baghchal.controller.StatusChanged
import baghchal.controller.CellChanged
import baghchal.controller.Exit
import org.specs2.specification.Scope
import org.specs2.mock.Mockito
import baghchal.logic.Game
import baghchal.logic.PlayerTiger
import baghchal.logic.PlayerGoat
import baghchal.logic.Cell
import baghchal.logic.util.Status
import baghchal.logic.util.Status._
import baghchal.logic.util.MoveResult._
import baghchal.logic.Tiger
import org.specs2.matcher.FileMatchers
import java.io.File

@RunWith(classOf[JUnitRunner])
class BaghChalControllerSpec extends Specification with Mockito with FileMatchers {
  sequential
  
	"A new controller" should {
	  "have the status text \"BaghChal\"" in new ControllerTest{
	    ctrl.statusText must beEqualTo("BaghChal")
	  }
	  
	  "have a block size of 5" in new ControllerTest{
	    ctrl.blockSize must beEqualTo(5)
	  }

      "print out the cells" in new ControllerTest{
        ctrl.reset
        ctrl.printGrid must beEqualTo(" tiger  empty  empty  empty  tiger \n empty  empty  empty  empty  empty \n empty  empty  empty  empty  empty \n empty  empty  empty  empty  empty \n tiger  empty  empty  empty  tiger \n")
      }
	}
	
	"A controller" should {
	  "know the Game" in new ControllerTest{
	    ctrl.game must beEqualTo(Game)
	  }
	  
	  "know the score" in new ControllerTest{
	    ctrl.score must beEqualTo(1)
	  }
	  
	  "know, how many goats have been placed" in new ControllerTest{
	    ctrl.goats must beEqualTo(5)
	  }
	  
	  "give the status of the goats" in new ControllerTest{
	    ctrl.statusGoats must beEqualTo("Goats to place: 15")
	  }
	  
	  "give the score" in new ControllerTest{
	    ctrl.statusScore must beEqualTo("Goats eaten: 1")
	  }

	  "call the correct cells" in new ControllerTest{
	    ctrl.cell(2, 2) must beEqualTo(Game.cells(2)(2))
	  }
	  
	  "reset the game and inform the views" in new ControllerReset{
	    ctrl.statusText must beEqualTo("BaghChal was reset")
	    listener.calledCellChanged must beTrue
	  }
	  
	  "update the views" in new ControllerTest{
	    ctrl.update
	    listener.calledCellChanged must beTrue
	    listener.calledStatusChanged must beTrue
	  }
	  
	  "Notify views, if one of them exited" in new ControllerTest{
	    ctrl.exit
	    listener.calledExit must beTrue
	  }
	}
	
	"When setting goats the controller" should {
	  "not accept out of bounds coordinates" in new ControllerTest{
	    ctrl.set(5, 5)
	    ctrl.statusText must beEqualTo("Invalid Move")
	  }
	  
	  "not accept set to occupied cells" in new SetTest{
	    ctrl.set(1, 2)
	    ctrl.statusText must beEqualTo("Invalid Move")
	  }
	  
	  "not accept set calls when it's the tigers turn" in new SetTest{
	    Game.turn = tiger
	    ctrl.set(2, 2)
	    ctrl.statusText must beEqualTo("Wrong Player")
	  }
	  
	  "Set the correct status and inform the views" in new SetTest{
	    ctrl.set(1, 1)
	    ctrl.statusText must beEqualTo("Set Goat to (1,1)")
	    listener.calledCellChanged must beTrue
	    
	    listener.reset
	    ctrl.set(2, 2)
	    ctrl.statusText must beEqualTo("Goat won!")
	    listener.calledCellChanged must beTrue
	  }
	}
	
	"When moving Avatars the controller" should {
	  "not move goats if less then 20 have been placed" in new MoveTest{
	    Game.turn = goat
	    ctrl.set(0,0,2,2)
	    ctrl.statusText must beEqualTo ("Invalid Move")
	  }
	  
	  "not accept out of bounds coordinates" in new MoveTest{
	    ctrl.set(-1,-1,5,5)
	    ctrl.statusText must beEqualTo("Invalid Move")
	  }
	  
	  "not accept moves to non-neighboring cells for tigers" in new MoveTest{
	    ctrl.set(0,0,3,3)
	    ctrl.statusText must beEqualTo("Invalid Move")
	  }
	  
	  "not accept moves to non-neighboring cells for goats" in new MoveTest{
	    ctrl.game.turn = goat
	    ctrl.set(1,2,1,4)
	    ctrl.statusText must beEqualTo("Invalid Move")
	  }
	  
	  "not accept moves to occupied cells" in new MoveTest{
	    ctrl.set(0,2,1,2)
	    ctrl.statusText must beEqualTo("Invalid Move")
	  }
	  
	  "not accept moves from unoccupied cells" in new MoveTest{
	    ctrl.set(1,0,2,0)
	    ctrl.statusText must beEqualTo("Invalid Move")
	  }
	  
	  "not accept moves by the wrong player" in new MoveTest{
	    
	    ctrl.set(1,2,1,3)
	    ctrl.statusText must beEqualTo("Wrong Player")
	  }
	  
	  "Set the correct status and inform the views" in new MoveTest{
	    ctrl.set(3, 3, 3, 4)
	    ctrl.statusText must beEqualTo("Invalid Move")
	    
	    ctrl.set(3, 3, 3, 2)
	    ctrl.statusText must beEqualTo("Move Tiger to (3,2)")
	    listener.calledCellChanged must beTrue
	    
	    Game.cells(3)(2).status returns tiger
	    listener.reset
	    ctrl.set(3, 2, 3, 0)
	    ctrl.statusText must beEqualTo("Move Tiger to (3,0)\nTiger ate goat (Score: 1)")
	    listener.calledCellChanged must beTrue
	    
	    Game.cells(3)(2).status returns Status.empty
	    Game.cells(3)(0).status returns tiger
	    listener.reset
	    ctrl.set(3, 0, 3, 2)
	    ctrl.statusText must beEqualTo ("Tiger won!")
	    listener.calledCellChanged must beTrue
	  }
	  
	  "recognize if the game is tied and inform the views" in new MoveTest{
	    Game.playerGoat.canMoveWithAGoat returns false
	    ctrl.set(3, 3, 3, 2)
	    ctrl.statusText must beEqualTo("No valid moves remaining. The Game is tied.")
	  }
  }
  
  "The FileIO-System" should{
    "save the game" in new SaveTest{
      val f = new File("savetest") 
      f must exist
      f.delete
    }
    
    "load the game" in new SaveTest{
      ctrl.load("savetest")
      Game.cells(0)(0).status must beEqualTo(Status.empty)
      Game.cells(0)(2).status must beEqualTo(tiger)
      Game.cells(2)(2).status must beEqualTo(goat)
      ctrl.score must beEqualTo(1)
      Game.turn must beEqualTo(tiger)
      val f = new File("savetest")
      f.delete
    }
  }
}

class BaghChalTestListener(controller:BaghChalController) extends Reactor{
  listenTo(controller)
  var calledExit = false
  var calledStatusChanged = false
  var calledCellChanged = false
  
  def reset = {
    calledExit = false
    calledStatusChanged = false
    calledCellChanged = false
  }
  
  
  reactions += {
    case Exit => calledExit = true
    case StatusChanged => calledStatusChanged = true
    case CellChanged => calledCellChanged = true
  }
}

trait ControllerTest extends Scope with Mockito{
  Game.reset
  val ctrl = new BaghChalController
  val listener = new BaghChalTestListener(ctrl)
  
  Game.playerTiger = mock[PlayerTiger]
  Game.playerTiger.score returns 1
  
  Game.playerGoat = mock[PlayerGoat]
  Game.playerGoat.placedGoats returns 5
  
  Game.cells = Array.tabulate(5,5)(createMockCell(_, _))
  
  def createMockCell(x:Int, y:Int) = {
    val c = mock[Cell]
    c.positionX returns x
    c.positionY returns y
    c
  }
}

trait ControllerReset extends ControllerTest{
  ctrl.reset
}

trait SetTest extends ControllerTest{
  Game.turn = goat
  Game.playerGoat.set(Game.cells(1)(1)) returns continues
  Game.playerGoat.set(Game.cells(2)(2)) returns ends
  Game.cells(1)(1).status returns empty
  Game.cells(1)(2).status returns goat
  Game.cells(2)(2).status returns empty
}

trait MoveTest extends ControllerTest{
  Game.playerGoat.placedGoats returns 20
  Game.turn = tiger
  Game.cells(0)(0).isNeighbor(Game.cells(3)(3)) returns false
  Game.cells(0)(0).isNeighbor(Game.cells(3)(3)) returns false
  Game.cells(1)(0).isNeighbor(Game.cells(2)(0)) returns true
  Game.cells(1)(2).isNeighbor(Game.cells(1)(4)) returns false
  Game.cells(0)(2).status returns tiger
  Game.cells(1)(2).status returns goat
  
  Game.cells(3)(3).status returns tiger
  val t = mock[Tiger]
  t.toString returns "Tiger"
  Game.cells(3)(3).avatar returns t
  Game.cells(3)(2).avatar returns t
  Game.cells(3)(0).avatar returns t
  t.move(Game.cells(3)(3), Game.cells(3)(4)) returns invalid
  t.move(Game.cells(3)(3), Game.cells(3)(2)) returns continues
  t.move(Game.cells(3)(2), Game.cells(3)(0)) returns goatEaten
  t.move(Game.cells(3)(0), Game.cells(3)(2)) returns ends
  
  Game.cells(3)(3).isNeighbor(Game.cells(3)(4)) returns true
  Game.cells(3)(3).isNeighbor(Game.cells(3)(2)) returns true
  Game.cells(3)(2).isNeighbor(Game.cells(3)(0)) returns false
  Game.cells(3)(2).isJumpOverNeighbor(Game.cells(3)(0)) returns true
  Game.cells(3)(0).isNeighbor(Game.cells(3)(2)) returns false
  Game.cells(3)(0).isJumpOverNeighbor(Game.cells(3)(2)) returns true
  
  Game.cells(1)(2).isNeighbor(Game.cells(1)(3)) returns true
  Game.cells(1)(2).isJumpOverNeighbor(Game.cells(1)(3)) returns false
  
  Game.cells(1)(3).status returns empty
  Game.cells(3)(4).status returns empty
  Game.cells(3)(2).status returns empty
  Game.cells(3)(0).status returns empty
  Game.cells(2)(0).status returns empty
  Game.playerGoat.canMoveWithAGoat returns true
  
  Game.cells(1)(0).status returns empty
}

trait SaveTest extends Scope {
  Game.reset
  val ctrl = new BaghChalController
  ctrl.set(0, 1)
  ctrl.set(0, 0, 0, 2)
  ctrl.set(2, 2)
  ctrl.save("savetest")
}