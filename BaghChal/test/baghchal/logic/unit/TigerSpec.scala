package baghchal.logic.unit

import org.specs2.mutable._
import org.specs2.runner._
import org.junit.runner._
import org.specs2.specification.Scope
import org.specs2.mock.Mockito
import baghchal.logic.util.Direction._
import baghchal.logic.util.Status._
import baghchal.logic.util.MoveResult._
import baghchal.logic.Cell
import baghchal.logic.Game
import baghchal.logic.Goat
import baghchal.logic.PlayerTiger
import baghchal.logic.Tiger

@RunWith(classOf[JUnitRunner])
class TigerSpec extends Specification {
  sequential
  
  "A Tiger" should {
    "be able to move, if a neighbor cell is empty" in new TigerWithEmptyNeighbor{
      t.canMove should beTrue
    }
    
    "be able to move, if a goat can be eaten" in new TigerWithEdibleGoatNeighbor{
      t.canMove should beTrue
    }
    
    "not be able to move, if all neighbor cells are occupied by tigers" in new TigerWithTigerNeighbor{
      t.canMove should beFalse
    }
    
    "not be able to move, if he cannot jump over neighboring goats" in new TigerWithNotEdibleGoatNeighbor{
      t.canMove should beFalse
    }
    
    "print out \"Tiger\"" in new NewTiger {
      t.toString must beEqualTo("Tiger")
    }
    
    "not move to an occupied cell" in new TigerWithTigerNeighbor{
      t.move(t.cell, n) must beEqualTo(invalid)
    }
    
    "eat a goat if he jumps over it" in new TestEatGoat{
      t.move(t.cell, nn) must beEqualTo(goatEaten)
    }
    
    "signal the end of game on PlayerTiger win" in new EatGoatAndGameEnds{
      t.move(t.cell, nn) must beEqualTo(ends)
    }
    
    "Move to an unoccupied cell" in new TigerWithEmptyNeighbor{
      t.move(t.cell, n) must beEqualTo(continues)
    }
  }
  
  /* Regression Test for issue #4 */
  "Jumping over a Tiger" should {
    "not be considered a valid move option" in new TestJumpOverTiger{
      t.canMove must beFalse
    }
  }
  /* END Regression Test for issue #4 */
}

trait NewTiger extends Scope {
  val t = new Tiger
}

trait TigerWithMockCell extends NewTiger with Mockito{
  t.cell = mock[Cell]
  t.cell.avatar returns t
  val n = mock[Cell]
  t.cell.neighborDirectionMap returns Map(n-> up)
  t.cell.neighbor(up) returns n
}

trait TigerWithJumpOverNeighbor extends TigerWithMockCell{
  val nn = mock[Cell]
}

trait TigerWithEmptyNeighbor extends TigerWithMockCell{
  n.status returns empty
}

trait TigerWithEdibleGoatNeighbor extends TigerWithJumpOverNeighbor{
  n.status returns goat
  nn.status returns empty
  n.neighbor(up) returns nn
}

trait TigerWithTigerNeighbor extends TigerWithMockCell{
  n.status returns tiger
}

trait TigerWithNotEdibleGoatNeighbor extends TigerWithJumpOverNeighbor{
  n.status returns goat
  nn.status returns goat
  n.neighbor(up) returns nn
}

trait TestEatGoat extends TigerWithEdibleGoatNeighbor{
  t.cell.isJumpOverNeighbor(nn) returns true
  t.cell.jumpOverDirection(nn) returns up
  
  val g = mock[Goat]
  
  n.avatar returns g
  g.cell returns n
}

trait TestJumpOverTiger extends TigerWithJumpOverNeighbor{ 
  n.status returns tiger
  nn.status returns empty
  n.neighbor(up) returns nn
}


trait EatGoatAndGameEnds extends TestEatGoat {
  Game.playerTiger = mock[PlayerTiger]
  Game.playerTiger.wins returns true
}

