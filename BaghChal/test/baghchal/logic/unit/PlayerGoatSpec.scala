package baghchal.logic.unit

import org.specs2.mutable._
import org.specs2.runner._
import org.junit.runner._
import org.specs2.specification.Scope
import org.specs2.mock.Mockito
import baghchal.logic.util.MoveResult._
import baghchal.logic.Cell
import baghchal.logic.Goat
import baghchal.logic.PlayerGoat
import baghchal.logic.Game

@RunWith(classOf[JUnitRunner])
class PlayerGoatSpec extends Specification with Mockito{
  sequential
  
  "A PlayerGoat" should {
	"have a list with twenty goats" in new NewPlayerG{
	  p.goats.length must equalTo(20)
	  p.goats must beAnInstanceOf[List[Goat]]
	}
	
	"be able to move with less than 20 goats set" in new NewPlayerG{
	  p.canMoveWithAGoat must beTrue
	}
	
	//unexplicably fails in coverage mode
	"be able to determine that moving is possible with all goats set" in new AllGoatsSetMovable{
	  p.placedGoats must be equalTo(20)
	  p.canMoveWithAGoat must beTrue
	}
	
	"be able to determine that moving is not possible with all goats set" in new AllGoatsSetImmovable{
	  p.placedGoats must be equalTo(20)
	  p.canMoveWithAGoat must beFalse
	}
  }
  
  "A new PlayerGoat" should {
    "have no placed goats" in new NewPlayerG{
      p.placedGoats must beEqualTo(0)
    } 
  }
  
  "Setting a Goat" should{    
    "increase the counter for placed goats" in new GoatSet{
      p.placedGoats must beEqualTo(1)
    }
    
    "pass up the return value from Goat::set" in new GoatMockSet{
      p.set(c) must beEqualTo(continues)
      p.set(c) must beEqualTo(ends)
      p.set(c) must beEqualTo(invalid)
    }
  }
}

trait NewPlayerG extends Scope{
  Game.reset
  val p = new PlayerGoat
}

trait GoatSet extends NewPlayerG {
  val c = new Cell(0,1)
  p.set(c)
}


trait GoatMockSet extends NewPlayerG with Mockito{
  val c = new Cell(0,1)
  p.goats = List.fill(20)(mock[Goat])
  p.goats(0).set(c) returns continues
  p.goats(1).set(c) returns ends
  p.goats(2).set(c) returns invalid
 }

trait AllGoatsSetMovable extends NewPlayerG with Mockito{
  p.placedGoats = 20
  p.goats = List.fill(20)(mock[Goat])
  p.goats.foreach(_.canMove returns false)
  p.goats(19).canMove returns true
}

trait AllGoatsSetImmovable extends NewPlayerG with Mockito{
  p.placedGoats = 20
  p.goats = List.fill(20)(mock[Goat])
  p.goats.foreach(_.canMove returns false)
}