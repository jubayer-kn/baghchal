package baghchal.logic.unit

import org.specs2.mutable._
import org.specs2.runner._
import org.junit.runner._
import org.specs2.specification.Scope
import org.specs2.mock.Mockito
import baghchal.logic.PlayerTiger
import baghchal.logic.Tiger

@RunWith(classOf[JUnitRunner])
class PlayerTigerSpec extends Specification {
  sequential
  
	"A new PlayerTiger" should{
	  "have a score of 0" in new NewPlayerT{
	    p.score must beEqualTo(0)
	  }
	  
	  "have a list of 4 tigers" in new NewPlayerT{
	    p.tiger must beAnInstanceOf[List[Tiger]]
	    p.tiger.length must beEqualTo(4)
	  }
	  
	  "not be winning" in new NewPlayerT{
	    p.wins must beFalse
	  }
	}
	
	"A PlayerTiger" should {
	  "be able to move if one tiger is able to move" in new MovableMockTigers{
	    p.canMoveWithATiger must beTrue
	  }
	  
	  "not be able to move if none of the tigers is able to move" in new ImmovableMockTigers {
	    p.canMoveWithATiger must beFalse
	  }
	  
	  "win if his score is 5" in new WinningTigers{
	    p.wins must beTrue
	  }
	}
}

trait NewPlayerT extends Scope {
  val p = new PlayerTiger
}

trait WinningTigers extends NewPlayerT{
  p.score = 5
}

trait MockTigers extends Scope with Mockito{
  val p = new PlayerTiger(List.fill(4)(mock[Tiger]))
}

trait MovableMockTigers extends MockTigers {  
  p.tiger(0).canMove returns false
  p.tiger(1).canMove returns false
  p.tiger(2).canMove returns false
  p.tiger(3).canMove returns true
}

trait ImmovableMockTigers extends MockTigers {
  p.tiger(0).canMove returns false
  p.tiger(1).canMove returns false
  p.tiger(2).canMove returns false
  p.tiger(3).canMove returns false
}