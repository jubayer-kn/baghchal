package baghchal.logic.unit

import org.specs2.mutable._
import org.specs2.runner._
import org.junit.runner._
import baghchal.logic.Game
import baghchal.logic.Cell
import baghchal.logic.Tiger
import org.specs2.specification.Scope
import baghchal.logic.util.Status
import baghchal.logic.util.Status._
import baghchal.logic.PlayerGoat
import org.specs2.mock.Mockito

@RunWith(classOf[JUnitRunner])
class GameSpec extends Specification {
    sequential 
    
	"The Game object" should{
	  "exist" in {
	    Game must not equalTo(null)
	  }
	  
	  "have a Player Tiger" in new ResetGame {
	    Game.playerTiger must not equalTo(null)
	  }
	  
	  "and a Player goat" in new ResetGame {
	    Game.playerGoat must not equalTo(null)
	  }
	  
	  
	  "recognize a tied game" in new MockTiedGame {
		  Game.isTied must equalTo(true)
	  }
	}
	
	"On reset the Game object" should{	  
	  "have the turn indicator set to goat" in new ResetGame{
	    Game.turn must equalTo(Status.goat)
	  }
	  
//	  "not be tied" in new ResetGame{
//	    Game.isTied must equalTo(false)
//	  }
//	  
	  "create a new 5x5 board with tigers in the four corners" in new ResetGame {
	    Game.cells.length must equalTo(5)
	    Game.cells(0).length must equalTo(5)
	    Game.cells(1).length must equalTo(5)
	    Game.cells(2).length must equalTo(5)
	    Game.cells(3).length must equalTo(5)
	    Game.cells(4).length must equalTo(5)
	    Game.cells(0)(0).avatar must haveClass[Tiger]
	    Game.cells(0)(4).avatar must haveClass[Tiger]
	    Game.cells(4)(0).avatar must haveClass[Tiger]
	    Game.cells(4)(4).avatar must haveClass[Tiger]
	  }
	}

    "The Grid object" should {
      "print out the cells" in new GridTest{
        Game.toString must beEqualTo(" tiger  empty  empty  empty  tiger \n empty  empty  empty  empty  empty \n empty  empty  goat   empty  empty \n empty  empty  empty  empty  empty \n tiger  empty  empty  empty  tiger \n")
      }
    }

}

trait ResetGame extends Scope {
  Game.reset
}

trait MockTiedGame extends Scope with Mockito {
  Game.reset
  Game.playerGoat = mock[PlayerGoat]
  Game.playerGoat.canMoveWithAGoat returns false
}

trait GridTest extends Scope with Mockito{
  Game.cells = Array.tabulate(5,5)(createMockCell(_, _))

  def createMockCell(x:Int, y:Int) = {
    val c = mock[Cell]
    c.positionX returns x
    c.positionY returns y
    if((x == 0 || x == 4) && (y == 0 || y == 4)) c.status returns tiger
    else if (x==2 && y == 2) c.status returns goat
    else c.status returns empty
    c
  }
}