package baghchal.logic.unit

import org.specs2.mutable._
import org.specs2.runner._
import org.junit.runner._
import org.specs2.specification.Scope
import org.specs2.mock.Mockito
import baghchal.logic.util.Status._
import baghchal.logic.util.Direction._
import baghchal.logic.util.Direction
import baghchal.logic.util.Status
import baghchal.logic.Cell
import baghchal.logic.Game
import baghchal.logic.Goat
import baghchal.logic.Tiger

@RunWith(classOf[JUnitRunner])
class CellSpec extends Specification with Mockito{
	sequential
	
	"A new cell" should{
	  "have no avatar" in new NewCell{
	    c.avatar must beNull
	  }
	}
	
	"A cell" should{
		"know its correct status" in new NewCell{
		  c.avatar = mock[Tiger]
		  c.status must beEqualTo(tiger)
		  c.avatar = mock[Goat]
		  c.status must beEqualTo(goat)
		  c.avatar = null
		  c.status must beEqualTo(Status.empty)
		}
		
		"know its neighbors" in new CellSet9{
		  cells(1)(1).neighbor(up) must beEqualTo(cells(1)(0))
		  cells(1)(1).neighbor(down) must beEqualTo(cells(1)(2))
		  cells(1)(1).neighbor(Direction.left) must beEqualTo(cells(0)(1))
		  cells(1)(1).neighbor(Direction.right) must beEqualTo(cells(2)(1))
		  
		  cells(1)(1).neighbor(leftUp) must beEqualTo(cells(0)(0))
		  cells(1)(1).neighbor(leftDown) must beEqualTo(cells(0)(2))
		  cells(1)(1).neighbor(rightDown) must beEqualTo(cells(2)(2))
		  cells(1)(1).neighbor(rightUp) must beEqualTo(cells(2)(0))
		}
		
		"not return a diagonal neighbor when there isn't one on the board" in new CellSet25{
		  cells(0)(1).neighbor(rightDown) must beNull
		}
		
		"not return an out of bounds neighbor" in new CellSet9{
		  cells(0)(1).neighbor(Direction.left) must beNull
		}
		
		"determine if another cell is a neighbor or not" in new CellSet9{
		  cells(0)(0).isNeighbor(cells(1)(1)) must beTrue
		  cells(0)(0).isNeighbor(cells(2)(2)) must beFalse
		}
		
		"determine if a tiger could jump over a goat to the neighbor of a neighbor" in new JumpOverTest{
		  cells(0)(0).isJumpOverNeighbor(cells(2)(2)) must beFalse
		  cells(0)(0).isJumpOverNeighbor(cells(2)(0)) must beTrue
		}
		
		"Know in which direction a neighbor lies (if it is one)" in new CellSet9{
		  cells(0)(0).neighborDirection(cells(2)(2)) must beNull
		  
		  cells(1)(1).neighborDirection(cells(0)(0)) must be(leftUp)
		  cells(1)(1).neighborDirection(cells(1)(0)) must be(up)
		  cells(1)(1).neighborDirection(cells(2)(0)) must be(rightUp)
		  cells(1)(1).neighborDirection(cells(0)(1)) must be(Direction.left)
		  
		  cells(1)(1).neighborDirection(cells(2)(1)) must be(Direction.right)
		  cells(1)(1).neighborDirection(cells(0)(2)) must be(leftDown)
		  cells(1)(1).neighborDirection(cells(1)(2)) must be(down)
		  cells(1)(1).neighborDirection(cells(2)(2)) must be(rightDown)
		}
		
		"Know in which direction a \"jump-over-neighbor\" lies" in new JumpOverTest {
		  cells(0)(0).jumpOverDirection(cells(2)(0)) must beEqualTo(Direction.right)
		}
		
		"Provide a map of its neighbors and their directions" in new CellSet9 {
		  val testMap = Map(
		      cells(0)(0) -> leftUp,
		      cells(1)(0) -> up,
		      cells(2)(0) -> rightUp,
		      cells(0)(1) -> Direction.left,
		      cells(2)(1) -> Direction.right,
		      cells(0)(2) -> leftDown,
		      cells(1)(2) -> down,
		      cells(2)(2) -> rightDown
		  )
		  
		  cells(1)(1).neighborDirectionMap must beEqualTo(testMap)
		}
		
		"Print out its coordinates" in new NewCell{
		  c.toString must beEqualTo("(0,0) ")
		}
	}
}

trait NewCell extends Scope {
  val c = new Cell(0,0)
}

trait CellSet9 extends Scope{
  val cells = Array.tabulate(3, 3)(  new Cell(_,_) )
  Game.cells = cells
}

trait CellSet25 extends Scope{
  val cells = Array.tabulate(5, 5)(  new Cell(_,_) )
  Game.cells = cells
}

trait JumpOverTest extends CellSet9 with Mockito{
  cells(1)(0).avatar = mock[Goat]
}