package baghchal.logic.unit

import org.specs2.mutable._
import org.specs2.runner._
import org.junit.runner._
import org.specs2.specification.Scope
import org.specs2.mock.Mockito
import baghchal.logic.util.MoveResult._
import baghchal.logic.util.Status._
import baghchal.logic.util.Direction._
import baghchal.logic.Cell
import baghchal.logic.Game
import baghchal.logic.Goat
import baghchal.logic.PlayerTiger

@RunWith(classOf[JUnitRunner])
class GoatSpec extends Specification {
	sequential
	
	"A new goat" should {
	  "not be eaten" in new NewGoat{
	    g.eaten must beFalse
	  }
	}
	
	"A goat" should {
	  "print \"Goat\"" in new NewGoat{
	    g.toString must beEqualTo("Goat")
	  }
	  
	  "be set to a cell" in new GoatWithMockCell{
	    g.set(c) must beEqualTo(continues)
	    g.cell must beEqualTo(c)
	  }
	  
	  "signal the end of game after setting a goat, if the tigers can't move" in new WinningGoat{
	    g.set(c) must beEqualTo(ends)
	  }
	  
	  "not move to an occupied cell" in new GoatWithOccupiedNeighbor{
	    g.move(g.cell, n) must beEqualTo(invalid)
	  }
	  
	  "move to an unoccupied cell" in new GoatWithUnoccupiedNeighbor{
	    g.move(g.cell, n) must beEqualTo(continues)
	    g.cell must beEqualTo(n)
	  }
	  
	  "signal the end of game after moving a goat, if the tigers can't move" in new WinningMoveGoat{
	    g.move(g.cell, n) must beEqualTo(ends)
	  }
	  
	  "not move if it's already been eaten" in new DeadGoat{
	    g.canMove must beFalse
	  }
	  
	  "move, if it's alive and has an unoccupied neighbor cell" in new GoatWithUnoccupiedNeighbor{
	    g.canMove must beTrue
	  }
	}
	
	"A dead goat" should{
	  "have been eaten" in new DeadGoat{
	    g.eaten must beTrue
	  } 
	  
	  "not have a cell" in new DeadGoat{
	    g.cell must beNull
	  }
	}
}

trait NewGoat extends Scope {
  Game.reset
  val g = new Goat
}

trait GoatWithMockCell extends NewGoat with Mockito {
  val c = mock[Cell]
}

trait WinningGoat extends GoatWithMockCell {
  Game.playerTiger = mock[PlayerTiger]
  Game.playerTiger.canMoveWithATiger returns false
}

trait GoatWithNeighbor extends GoatWithMockCell{
  g.cell = c
  val n = mock[Cell]
  
  /* Needed to keep coverage test from failing. Doesn't really serve a purpose here */
  Game.playerTiger = mock[PlayerTiger]
  Game.playerTiger.canMoveWithATiger returns true
}

trait GoatWithOccupiedNeighbor extends GoatWithNeighbor{
  n.status returns goat
}

trait GoatWithUnoccupiedNeighbor extends GoatWithNeighbor{
  n.status returns empty
  
  c.neighborDirectionMap returns Map(n -> up)
  c.neighbor(up) returns n
  n.neighbor(down) returns c
}

trait WinningMoveGoat extends GoatWithUnoccupiedNeighbor with WinningGoat{}

trait DeadGoat extends NewGoat{
  g.die
}