/**
* This script serves as the websocket handler
*
* @author sischnee <sischnee@gmail.com>
* @since 2014/01/03
* @license BSD common license
*/

var BG = BG || {};
BG.htwg = BG.htwg || {};

BG.htwg.Websocket = function($){

 /**
  * Contains the webSocket
  *
  * @var
  * @access public
  * @type object
  */
  this._websocket = false;

  this.openEvent = function(event){
      BG.htwg.game.redrawInit();
      BG.htwg.game.informInit();
  };

  this.sendMessage = function( msg ) {
    this._websocket.send( JSON.stringify( msg ) );
  };

  this.receiveEvent = function(event) {
    var data = JSON.parse(event.data);

    // Handle errors
    if(data.error) {
      //$("#onError span").text(data.error)
      //$("#onError").show()
      
      //alert(data.text)
      alert("Error");
      return;
    }
    else {
     //handle to the right class
      switch (data.type) {
        case "game":
          BG.htwg.game.executeCommand( data );
          break;
        default:break;
      }  
    }        
  };

};
