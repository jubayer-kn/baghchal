/**
* This script shows error annotations in the ace editor
*
* @author sischnee <sischnee@gmail.com>
* @since 2014/01/03
* @license BSD common license
*/

var BG = BG || {};
BG.htwg = BG.htwg || {};

BG.htwg.Game = function($){

  if ( jQuery('#game').length == 0 ) {
      //no need to initialize
      return false;
  }
  
  /**
   * Scope duplicator / parent this
   *
   * @var
   * @access private
   * @type object
   */
   var that = this;

   this.placedGoatsCount = 0;
   this.turn = "goat";
   this.score = 0;

   this.init = function(){

       $("#new").click(function(){
           this.placedGoatsCount = 0;
           this.turn = "goat";
           this.score = 0;
           BG.htwg.websocket.sendMessage({"type":"game", "command":"new"})
       });

       $("#help").click(function(){
           $("#help_link").click();
       });

       $("#info").click(function(){
           $("#info_link").click();
       });

       $(".cell").droppable({
           accept: ".avatar",
           hoverClass: "ui-state-highlight",
           drop: that.handleDrop
       });
   }

   this.handleDrop = function(event, ui){
       var object = JSON.parse($(this).attr("data-field"));
       var toRow = object.row;
       var toColumn = object.column;
       if ( that.placedGoatsCount < 20 && that.turn == "goat" && !$(ui.draggable).parent().hasClass("cell")){
           $(ui.draggable).remove();
           BG.htwg.websocket.sendMessage({"type":"game", "command":"set","exec":toRow.toString()+toColumn.toString()})
       }else if ($(ui.draggable).parent().hasClass("cell")){
           var object = JSON.parse($(ui.draggable).parent().attr("data-field"));
           var fromRow = object.row;
           var fromColumn = object.column;
           $(ui.draggable).remove();
           BG.htwg.websocket.sendMessage({"type":"game", "command":"set","exec":fromRow.toString()+fromColumn.toString()+toRow.toString()+toColumn.toString()})
       }
   }

   //probably there might be more commands
   this.executeCommand = function(data){
     switch ( data.command ){
       case "redrawInit":
         this.redrawInit();
         break;
       case "redraw":
         this.redraw(data)
         break;
       case "inform":
         this.inform(data);
         break;
       case "error":
         console.log(data.text)
         break;
       default:break;
     }
   };

   this.informInit = function(){
       BG.htwg.websocket.sendMessage({"type":"game", "command":"inform"});
   };

   this.inform = function(msg){
       $("#status").html(msg.status);
       this.placedGoatsCount = parseInt(msg.placedGoatsCount);
       this.turn = msg.turn;
       this.score = msg.score;
       var toPlace = 20 - this.placedGoatsCount;
       $("#goats").empty();

       for(var i = 0; i < Math.floor(toPlace/2); i++){
           var row = $("<tr><td><div class='avatar'></div></td><td><div class='avatar'></div></td></tr>");
           $("#goats").append(row);
       }
       if ( toPlace%2 ){
           var row = $("<tr><td><div class='avatar'></div></td></tr>");
           $("#goats").append(row);
       }

       $(".avatar").draggable({
           cursor: 'pointer',
           revert: true,
           revertDuration: 10
       });

       for(var i = 0; i < 5; i++){
           if ( i < this.score ){
               if ( $("#tiger_"+i).children(".avater").length == 0 ){
                   var avatar = $("<div class='avatar'></div>");
                   avatar.css("background-image","url('assets/images/spielfigur_ziege.png')");
                   $("#tiger_"+i).html(avatar);
               }
           }else{
               $("#tiger_"+i).empty();
           }
       }

       if ( this.turn == "goat"){
           $("#turnGoat").css("display","block");
           $("#turnTiger").css("display","none");
       }

       if ( this.turn == "tiger"){
           $("#turnGoat").css("display","none");
           $("#turnTiger").css("display","block");
       }

       //if drag and drop is an invalid move, the dragged avatar has to be redrawed
       //cellchanged in the controller is not sufficient
       BG.htwg.game.redrawInit();
   };

   this.redrawInit = function(){
       $.each($(".cell"), function(i,cell){
           var object = JSON.parse($(cell).attr("data-field"));
           var row = object.row;
           var column = object.column;
           BG.htwg.websocket.sendMessage({"type":"game", "command":"redraw", "row":row.toString(), "column":column.toString()})
       });
   };

   this.redraw = function(msg){
        var cell = $("#cell_"+msg.row+msg.column);
        if ( msg.status != "empty"){
            var avatar = null;
            if ( cell.children(".avatar").length == 0 ){
                 avatar = $("<div class='avatar'></div>");
            }else{
                avatar = cell.children(".avatar");
            }

            if ( msg.status == "goat" && avatar.css("background-image") != "url('assets/images/spielfigur_ziege.png')" ){
                avatar.css("background-image","url('assets/images/spielfigur_ziege.png')");
            }
            if ( msg.status == "tiger" && avatar.css("background-image") != "url('assets/images/spielfigur_tiger.png')"){
                avatar.css("background-image","url('assets/images/spielfigur_tiger.png')");
            }
            avatar.draggable({
                cursor: 'pointer',
                revert: true,
                revertDuration: 10
            });
            if ( cell.children(".avatar").length == 0 ){
                cell.html(avatar);
            }
        }else{
            cell.empty();
        }
   };

   this.init();
  
}