package baghchal.util.fileio

import java.io.FileInputStream
import java.io.ObjectInputStream
import baghchal.logic.Game
import baghchal.logic.Cell
import baghchal.logic.PlayerGoat
import baghchal.logic.PlayerTiger
import baghchal.game.BaghChal
import baghchal.controller.BaghChalController
import java.io.FileNotFoundException
import java.io.File
import baghchal.logic.util.Status

class BaghChalLoader(val file:File) {
	val fis = new FileInputStream(file)
	val ois = new ObjectInputStream(fis)
	
	def load = {
	    Game.cells = ois.readObject.asInstanceOf[Array[Array[Cell]]]
	    Game.playerGoat = ois.readObject.asInstanceOf[PlayerGoat]
	    Game.playerTiger = ois.readObject.asInstanceOf[PlayerTiger]
	    Game.turn = ois.readObject.asInstanceOf[Status.Value]
	}
}