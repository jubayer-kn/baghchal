package baghchal.util.fileio

import java.io.ObjectOutputStream
import java.io.FileOutputStream
import baghchal.logic.Game
import java.io.File

class BaghChalSaver(val file:File) {

	val fos = new FileOutputStream(file)
	val oos = new ObjectOutputStream(fos)
	
	def save = {
	  oos.writeObject(Game.cells)
	  oos.writeObject(Game.playerGoat)
	  oos.writeObject(Game.playerTiger)
	  oos.writeObject(Game.turn)
	}
}