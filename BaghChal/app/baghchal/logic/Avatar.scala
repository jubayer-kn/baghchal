package baghchal.logic

import baghchal.logic.util.MoveResult
import baghchal.logic.util.MoveResult._
import baghchal.logic.util.Status
@SerialVersionUID(1L)
abstract class Avatar extends Serializable{
  
    var cell: Cell = new Cell(-1,-1)
    
    def move(oldCell:Cell, newCell:Cell):MoveResult
}

@SerialVersionUID(1L)
case class Tiger() extends Avatar{
  
	def canMove = moveOptions contains true
	
	private def moveOptions = {
	  (for{
	      (neighbor, direction) <- cell.neighborDirectionMap
	   } yield (neighbor.status == Status.empty || 
	       (neighbor.status == Status.goat && neighbor.neighbor(direction) != null && neighbor.neighbor(direction).status == Status.empty))
	  ).toList
	}
	
	private def eatGoat(goatCell:Cell) = {
	  goatCell.avatar.asInstanceOf[Goat].die
	  Game.playerTiger.score += 1
	}
	
	override def toString = "Tiger"
	  
	def move(oldCell:Cell, newCell:Cell):MoveResult = {	  
      if ( newCell.status != Status.empty ){
	    invalid
	  }else{
	    oldCell.avatar = null	    
	    newCell.avatar = this
		this.cell = newCell
		  
		Game.turn = Status.goat
		  
		if(oldCell isJumpOverNeighbor newCell){
		  val direction = oldCell.jumpOverDirection(newCell)
		  val goatCell = oldCell.neighbor(direction)
		  eatGoat(goatCell)
		  if(Game.playerTiger.wins) ends else goatEaten
		}else{
		  continues
		}
	  }
	}
}

@SerialVersionUID(1L)
case class Goat() extends Avatar {
	  		
	var eaten: Boolean = false
	
	override def toString = "Goat"
	  
	def set(cell:Cell) = {
	  cell.avatar = this
	  this.cell = cell
	  Game.turn = Status.tiger
	    
	  moveResult
	}
	
	def move(oldCell:Cell, newCell:Cell):MoveResult = {
	  if ( newCell.status != Status.empty ){
	    MoveResult.invalid
	  }else{
	    oldCell.avatar = null
	    newCell.avatar = this
		this.cell = newCell
		Game.turn = Status.tiger  
		  
		moveResult
	  }
	}
	
	private def moveResult = if ( Game.playerTiger.canMoveWithATiger ) continues else ends
	
	def canMove = {
		!eaten && (moveOptions contains true)
	}
	
	private def moveOptions = {
	  (for{
	      (neighbor, direction) <- cell.neighborDirectionMap
	   } yield (neighbor.status == Status.empty)
	  ).toList
	}
	
	def die = {
	  eaten = true
	  cell.avatar = null
	  cell = null
	}
}