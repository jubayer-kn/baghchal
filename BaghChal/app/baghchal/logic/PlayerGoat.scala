package baghchal.logic

import baghchal.logic.util.MoveResult._

@SerialVersionUID(1L)
class PlayerGoat extends Serializable{

  var goats = List.fill(20)(new Goat)
  
  var placedGoats = 0
  
  def set( cell: Cell ):MoveResult = {
    val res =  goats(placedGoats).set(cell)
    placedGoats = placedGoats + 1
    
    res
  }
  
  def canMoveWithAGoat = placedGoats < 20 || ((for(g <- goats) yield g.canMove) contains true)
  
}