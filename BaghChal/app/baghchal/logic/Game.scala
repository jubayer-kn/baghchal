package baghchal.logic

import baghchal.logic.util.Status

object Game {
  var playerTiger = new PlayerTiger
  var playerGoat = new PlayerGoat
  var cells = createCells
  
  private def createCells = {
    var cells = Array.tabulate(5, 5)(  new Cell(_,_) )
    cells(0)(0).avatar = playerTiger.tiger(0)
    playerTiger.tiger(0).cell = cells(0)(0)
    cells(0)(4).avatar = playerTiger.tiger(1)
    playerTiger.tiger(1).cell = cells(0)(4)
    cells(4)(0).avatar = playerTiger.tiger(2)
    playerTiger.tiger(2).cell = cells(4)(0)
    cells(4)(4).avatar = playerTiger.tiger(3)
    playerTiger.tiger(3).cell = cells(4)(4)
    cells
  }
 
  var turn:Status.Value = Status.goat
  
  def reset{
    playerTiger = new PlayerTiger
	playerGoat = new PlayerGoat
    cells = createCells
	turn = Status.goat
  }
  
  def isTied = !playerGoat.canMoveWithAGoat

  override def toString = {
    var grid: String = ""
    for(i <- 0 until 5){
      for( j <- 0 until 5){
        grid = grid + " " + cells(i)(j).status.toString + " "
        if ( cells(i)(j).status.toString.equals("goat") ) grid = grid + " "
        if ( j == 4 ) grid = grid + "\n"
      }
    }
    grid
  }
}