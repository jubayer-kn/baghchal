package baghchal.logic

@SerialVersionUID(1L)
class PlayerTiger(val tiger:List[Tiger]) extends Serializable{
  var score: Int = 0
  
  def this() = {
    this(List.fill(4)(new Tiger))
  }
  
  def canMoveWithATiger = (for(t <- tiger) yield t.canMove) contains true
  
  def wins = score >= 5
}