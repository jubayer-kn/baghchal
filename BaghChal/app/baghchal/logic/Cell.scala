package baghchal.logic

import baghchal.logic.util.Direction
import baghchal.logic.util.Direction._
import baghchal.logic.util.Status

@SerialVersionUID(1L)
class Cell (val positionX:Int, val positionY:Int) extends Serializable{

  var avatar:Avatar = null
   
  def status = {
    avatar match {
      case Tiger() => Status.tiger
      case Goat()  => Status.goat
      case _       => Status.empty
    }
  }
  
  def neighbor(direction:Direction.Value) = {
    if((Direction.diagonals contains direction) && !canMoveDiagonally){
      null
    }else if (this isBoundary direction){
      null 
    }else{
      direction match {
	    case `rightUp`   => Game.cells(positionX + 1)(positionY - 1)
	    case `leftUp`	 => Game.cells(positionX - 1)(positionY - 1)
	    case `rightDown` => Game.cells(positionX + 1)(positionY + 1)
	    case `leftDown`  => Game.cells(positionX - 1)(positionY + 1)
	    case `up`        => Game.cells(positionX)(positionY - 1)
	    case `left`      => Game.cells(positionX - 1)(positionY)
	    case `down`      => Game.cells(positionX)(positionY + 1)
	    case `right`     => Game.cells(positionX + 1)(positionY)
	  }
    }
  }
  
  private def canMoveDiagonally = {
    (positionX % 2) == (positionY % 2)
  }
  
  private def isBoundary(direction:Direction) = {
    direction match {
      case `rightUp`   => positionX == 4 || positionY == 0
      case `leftUp`    => positionX == 0 || positionY == 0
      case `rightDown` => positionX == 4 || positionY == 4
      case `leftDown`  => positionX == 0 || positionY == 4
      case `up`        => positionY == 0
      case `left`      => positionX == 0
      case `down`      => positionY == 4
      case `right`     => positionX == 4
    }
  }
  
  def isNeighbor(cell:Cell) = {
      neighbors contains cell
  }
  
  private def neighbors = (for(d <- allDirections) yield neighbor(d)).filter(_ != null)   
  
  def isJumpOverNeighbor(cell:Cell) = {
    val jumpOverPossibilities = (for(n <- neighborDirectionMap) yield (n._1.status == Status.goat && n._1.neighbor(n._2) == cell)).toList
    jumpOverPossibilities contains true
  }
  
  def neighborDirection(neighbor:Cell) = {
    if(!isNeighbor(neighbor)){
      null
    }else{
      neighborDirectionMap(neighbor)
    }
  }
  
  def jumpOverDirection(target:Cell) = {
    var direction:Direction = null
    for(n <- neighborDirectionMap){
	  if(n._1.status == Status.goat && n._1.neighbor(n._2) == target){
	    direction = n._2  
	  }
    }
    direction
  }
  
  def neighborDirectionMap = {
    (for (d <- allDirections) yield (neighbor(d),d)).filter(_._1 != null).toMap
  }
  
  override def toString = "(" + positionX + "," + positionY +") "
}