package baghchal.logic.util

object Direction extends Enumeration {
	type Direction = Value
	val right,left,up,down,leftUp,rightUp,leftDown,rightDown = Value
	val allDirections = List(right,left,up,down,leftUp,rightUp,leftDown,rightDown)
	val diagonals = List(leftUp, rightUp, leftDown, rightDown)
}