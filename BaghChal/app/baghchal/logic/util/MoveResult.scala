package baghchal.logic.util

object MoveResult extends Enumeration {
	type MoveResult = Value
	val invalid,continues,ends,goatEaten = Value
}