package baghchal.game

import baghchal.view.Tui
import baghchal.controller.BaghChalController
import baghchal.view.SwingGui

object BaghChal {
    val controller = new BaghChalController
    val tui = new Tui(controller)
    val gui = new SwingGui(controller)
    
	def main(args: Array[String]) {
	          while (tui.processInputLine(readLine())) {}
    }
}