package baghchal.view

import scala.swing.Frame
import java.awt.Color
import java.awt.Dimension
import scala.swing.TabbedPane
import scala.swing.Label
import scala.swing.ScrollPane
import scala.io.Source
import java.awt.Font
import scala.swing.Button


class SwingHelp extends Frame{
	title="BaghChal Help"
	background = Color.red
	preferredSize = new Dimension(500,500)
	val labelFont = new Font("label", Font.PLAIN, 12)
	
	val usageTab = new ScrollPane{
	  contents = new Label{
	    text = Source.fromFile("app/baghchal/help/usage.html").mkString
	    font = labelFont
	  }
	}
	
	val rulesTab = new ScrollPane{
	  horizontalScrollBarPolicy = ScrollPane.BarPolicy.Never
	  contents = new Label{
	    text = Source.fromFile("app/baghchal/help/rules.html").mkString
	    font = labelFont
	  }
	}
	
	contents = new TabbedPane{
	  pages += new TabbedPane.Page("Usage",usageTab)
	  pages += new TabbedPane.Page("Rules", rulesTab)
	}
}