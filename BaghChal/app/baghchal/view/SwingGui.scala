package baghchal.view

import baghchal.view.util.ImagePanel
import scala.swing.FlowPanel
import baghchal.controller.BaghChalController
import scala.swing.Frame
import scala.swing.Button
import scala.swing.TextField
import scala.swing.event.ButtonClicked
import scala.swing.BorderPanel
import java.awt.Dimension
import javax.swing.ImageIcon
import java.awt.Color
import scala.swing.MenuBar
import baghchal.logic.util.Status
import scala.swing.Menu
import scala.swing.event.Key
import baghchal.view.util.BaghChalButton
import scala.swing.MenuItem
import scala.swing.Action
import baghchal.controller.CellChanged
import baghchal.controller.StatusChanged
import scala.swing.FileChooser
import java.io.File
import scala.swing.Dialog
import baghchal.controller.Exit
import javax.swing.Icon


class SwingGui(controller: BaghChalController) extends Frame {

  listenTo(controller)

  title = "BaghChal"

  background = Color.white
  preferredSize = new Dimension(500,600)
    
  val statusline = new TextField(controller.statusText, 20)
  val score = new TextField(controller.statusScore, 10)
  val goats = new TextField(controller.statusGoats, 10)
  val help = new SwingHelp
  var fromRow = -1
  var fromColumn = -1
  var clicked = false
  
  var cells = Array.ofDim[BaghChalButton](controller.blockSize, controller.blockSize)
    
  def infoPanel = new FlowPanel{
    background = new Color(85,85,255)
	preferredSize = new Dimension(430,50)
    contents += goats
    contents += score
  } 
    
  val gamePanel = new FlowPanel {
    background = new Color(85,85,255)
    preferredSize = new Dimension(407,500)
	val imagePanel =  new ImagePanel(5,5){   
      preferredSize = new Dimension(407,407)
	  imagePath = ("public/images/grid_background.png")
	  hGap = 35
      vGap = 35
	  for (row <- 0 until controller.blockSize; column <- 0 until controller.blockSize) {
          val button = new BaghChalButton(row, column, controller){
        	  preferredSize = new Dimension(20,20)
        	  redraw
        	  reactions += {
		        case ButtonClicked(_) => {
		          if ( controller.goats < 20 && controller.game.turn == Status.goat){
		            controller.set(row,column)
		          }  
		          else{
		            if (clicked){
		              controller.set(fromRow, fromColumn, row,column)
		              fromRow = -1
		              fromColumn = -1
		              clicked = false
		            }else{
		              if ( controller.checkFromCell(row, column) ){
			              clicked = true
			              fromRow = row
			              fromColumn = column
		              }
		            }
		          }
		          inform
		        }
		      }
          }
          cells(row)(column) = button
          contents+= button
          listenTo(button)
      }
    }
	contents += imagePanel
  }
  
  contents = new BorderPanel {
    add(infoPanel, BorderPanel.Position.North)
    add(gamePanel, BorderPanel.Position.Center)
    add(statusline, BorderPanel.Position.South)
  }
  
  menuBar = new MenuBar {
    contents += new Menu("File") {
      mnemonic = Key.F
      contents += new MenuItem(Action("New") { controller.reset })
      contents += new MenuItem(Action("Save") { saveFile })
      contents += new MenuItem(Action("Load") { openFile })
      contents += new MenuItem(Action("Quit") { controller.exit })
    }
    contents += new Menu("Help") {
      mnemonic = Key.H
      contents += new MenuItem(Action("Help") { showHelp })
      contents += new MenuItem(Action("Info") { openInfoWindow })
    }
  }
  
  resizable = false
  visible = true
  
  reactions += {
    case CellChanged => redraw
    case StatusChanged => refreshStatus
    case Exit => System.exit(0)
  }
  
  def inform = {
    score.text = controller.statusScore
    goats.text = controller.statusGoats
  }
  
  def redraw = {
    inform
    for (row <- 0 until controller.blockSize; column <- 0 until controller.blockSize) {
	  cells(row)(column).redraw
    }  
  }
  
  def refreshStatus = {
    statusline.text = controller.statusText
  }
  
  def saveFile = {
    val chooser = fileChooser
    chooser.showSaveDialog(gamePanel) match {
      case FileChooser.Result.`Approve` => controller.save(chooser.selectedFile)
      case FileChooser.Result.`Error`   => Dialog.showMessage(null,"An Error Occured", "Error")
       case FileChooser.Result.`Cancel`	=>
    }    
  }
  
  def openFile = {
    val chooser = fileChooser
    chooser.showOpenDialog(gamePanel) match {
      case FileChooser.Result.`Approve` => controller.load(chooser.selectedFile)
      case FileChooser.Result.`Error`   => Dialog.showMessage(null,"An Error Occured", "Error")
      case FileChooser.Result.`Cancel`	=>
    }
  }
  
  private def fileChooser = new FileChooser(new File("."))
  
  def openInfoWindow = {
    Dialog.showMessage(gamePanel, "BaghChal 2.0\n\n is being developed at University of Applied Sciences Constance as a software project.\n\nDevelopers:\n Simon Schneeberger\n Julian Bayer", "About BaghChal", Dialog.Message.Info, new ImageIcon("public/images/Logo.png"))
  }  
  
  def showHelp = {
    help.open
  }
}