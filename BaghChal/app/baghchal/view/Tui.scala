package baghchal.view

import baghchal.controller.BaghChalController
import scala.swing.Reactor
import baghchal.controller.CellChanged
import baghchal.controller.StatusChanged
import baghchal.controller.Exit

class Tui(var controller: BaghChalController) extends Reactor {
  listenTo(controller)
  printTui
  
  var continue = true
  
  def update = printTui
  
  def printTui = {
    println(controller.printGrid)
    println("Enter command: q-Quit n-New s-Save l-Load ab-SetGoat abcd-MoveAvatar (a:fromRow, b:fromColumn, c:toRow, d:toColumn)")
  }
  
  def printStatus = {
    println(controller.statusText)
  }
  
  def processInputLine(input: String) = {
    input match {
      case "q" => controller.exit
      case "n" => {
        controller.reset
        update
      }
      case "s" => {
    	  controller.save(readFileName)
      }
      case "l" => {
          controller.load(readFileName)
      }
      case _ => {
        input.toList.filter(c => c != ' ').map(c => c.toString.toInt) match {
          case toRow :: toColumn :: Nil =>{
            controller.set(toRow, toColumn)
            update
          } 
          case fromRow :: fromColumn :: toRow :: toColumn :: Nil =>{
            controller.set(fromRow, fromColumn, toRow, toColumn)
            update
          } 
          case _ => println("False Input!!!")
        }
      }
    }
    continue
  }
  
  private def readFileName = {
    println("Please specify a file name:")
    readLine
  }
  
  reactions += {
    case CellChanged => printTui
    case StatusChanged => printStatus
    case Exit => continue = false
  }
}