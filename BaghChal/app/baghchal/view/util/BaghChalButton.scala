package baghchal.view.util

import scala.swing.Button
import baghchal.controller.BaghChalController
import javax.swing.ImageIcon
import baghchal.logic.util.Status

class BaghChalButton(x:Int, y:Int, controller: BaghChalController) extends Button{
  
  def redraw = {
      icon = null
      if ( controller.game.cells(x)(y).status == Status.tiger ) icon = new ImageIcon("public/images/spielfigur_tiger.png")
      if ( controller.game.cells(x)(y).status == Status.goat ) icon = new ImageIcon("public/images/spielfigur_ziege.png")
  }
  
}