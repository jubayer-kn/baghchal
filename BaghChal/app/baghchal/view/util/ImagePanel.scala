package baghchal.view.util

import swing._                                                                

import java.awt.image.BufferedImage                                           
import java.io.File                                                           
import javax.imageio.ImageIO                                                  

class ImagePanel(rows0:Int, cols0:Int) extends GridPanel(rows0, cols0)                                                
{                                                                             
  private var _imagePath = ""                                                 
  private var bufferedImage:BufferedImage = null                              

  def imagePath = _imagePath                                                  

  def imagePath_=(value:String)                                               
  {                                                                           
    _imagePath = value                                                        
    bufferedImage = ImageIO.read(new File(_imagePath))                        
  }                                                                           


  override def paintComponent(g:Graphics2D) =                                 
  {                                                                           
    if (null != bufferedImage) g.drawImage(bufferedImage, 0, 0, null)         
  }                                                                           
}                                                                             

object ImagePanel                                                             
{                                                                             
  def apply() = new ImagePanel(5,5)                                              
} 