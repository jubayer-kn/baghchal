package baghchal.controller

import scala.swing.event.Event
import scala.swing.Publisher
import baghchal.logic.Game
import baghchal.logic.util.Status
import baghchal.logic.util.MoveResult._
import baghchal.logic.Cell
import baghchal.util.fileio.BaghChalSaver
import baghchal.util.fileio.BaghChalLoader
import java.io.FileNotFoundException
import java.io.File

case object CellChanged extends Event
case object StatusChanged extends Event
case object Exit extends Event

class BaghChalController() extends Publisher{
    var statusText="BaghChal"
    val game = Game
    def score = Game.playerTiger.score
    def goats = Game.playerGoat.placedGoats
    def statusGoats = "Goats to place: " + ( 20 - goats )
    def statusScore = "Goats eaten: " + score
      
    val blockSize = 5
      
    def cell(row: Int, col: Int) = Game.cells(row)(col)
    
    def reset = {
	    Game.reset
	    changeStatus("BaghChal was reset")
	    publish(CellChanged)
    }
    
    def set(toRow: Int, toCol: Int):Unit = {
	    if ( isInvalidMove(toRow,toCol)){
		    changeStatus("Invalid Move")
	    }else if( Game.turn == Status.tiger ){
	        changeStatus("Wrong Player")
	    }else{	  
	      val result = Game.playerGoat.set(Game.cells(toRow)(toCol))
	      result match {
	        case `continues` => changeStatus("Set Goat to ("+ toRow+","+toCol+")")
	        case `ends`	   => { 
	          changeStatus("Goat won!")
	          Game.reset
	        }
	      }
	      publish(CellChanged)
	    }
    }
    
    private def isInvalidMove(toRow:Int, toCol:Int) = {
      (Game.playerGoat.placedGoats >= 20 ||
	  toRow > 4 || toRow < 0 ||
	  toCol > 4 || toCol < 0 ||
	  Game.cells(toRow)(toCol).status != Status.empty)
    }
    
    def set(fromRow: Int, fromCol: Int, toRow: Int, toCol: Int):Unit = {
        if ( (Game.playerGoat.placedGoats < 20 && Game.turn == Status.goat) || coordinatesAreOutOfBounds(fromRow, fromCol, toRow, toCol) ){
            changeStatus("Invalid Move")
        }else if ( !cellsAreNeighbors(cell(fromRow,fromCol), cell(toRow,toCol)) || Game.cells(toRow)(toCol).status != Status.empty ){
            changeStatus("Invalid Move")
        }else if ( checkFromCell(fromRow, fromCol) ){
            val oldCell = Game.cells(fromRow)(fromCol)
            val newCell = Game.cells(toRow)(toCol)
            val avatar = oldCell.avatar 
        
            val result:MoveResult = avatar.move(oldCell, newCell)

            result match {
            	case `invalid`	=> changeStatus("Invalid Move")
            	case `continues` 	=> {
            	    if(Game.isTied){
            	        Game.reset
            	        changeStatus("No valid moves remaining. The Game is tied.")
            	    }else{
            	        changeStatus("Move " + avatar.toString + " to ("+ toRow+","+toCol+")")
            	    }
            	    publish(CellChanged)
            	}
            	case `goatEaten`  => {
            	  changeStatus("Move " + avatar.toString + " to ("+ toRow+","+toCol+")" + "\n" + avatar.toString + " ate goat (Score: " + Game.playerTiger.score + ")")
            	  publish(CellChanged)
            	}
            	case `ends`		=> {
            		Game.reset
            		changeStatus(avatar.toString() + " won!")
            		publish(CellChanged)
            	}
            }
        }     
    }
    
    private def coordinatesAreOutOfBounds(fromRow:Int, fromCol:Int, toRow:Int, toCol:Int) = {
      ( fromRow > 4 || fromRow < 0 ||
      fromCol > 4 || fromCol < 0 ||
      toRow > 4 || toRow < 0 ||
      fromCol > 4 || toCol < 0 )
    }
    
    private def cellsAreNeighbors(from:Cell,to:Cell) = {
      if(Game.turn == Status.goat) from isNeighbor to
      else (from isNeighbor to) || (from isJumpOverNeighbor to)
    }
    
    def checkFromCell(fromRow:Int, fromCol:Int): Boolean = {
        if ( Game.cells(fromRow)(fromCol).status == Status.empty ){
            changeStatus("Invalid Move")
    	    false
        }else if ( ( Game.cells(fromRow)(fromCol).status == Status.tiger && Game.turn == Status.goat ) ||
             ( Game.cells(fromRow)(fromCol).status == Status.goat && Game.turn == Status.tiger ) ){
            changeStatus("Wrong Player")
            false
        }else true
    }
    
    private def changeStatus(text:String) = {
      statusText=text
      publish(StatusChanged)
    }
    
    def save(fileName:String):Unit={
      save(new File(fileName))
    }
    
    def save(file:File) = {
      val saver = new BaghChalSaver(file)
      saver.save
      changeStatus("Game saved.")
    }
    
    def load(fileName:String):Unit = {
      load(new File(fileName))
    }
    
    def load(file:File) = {
      try{
        val loader = new BaghChalLoader(file)
        loader.load
        changeStatus("Game loaded.")
        publish(CellChanged)
      }catch{
        case fnf:FileNotFoundException => changeStatus("The file you specified was not found.")
        case e:Exception => changeStatus("An error occured while reading the file.")
      }
    }
    
    def update = {
      publish(CellChanged)
      publish(StatusChanged)
    }
    
    def exit = publish(Exit)

    def printGrid = Game.toString
}