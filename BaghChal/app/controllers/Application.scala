package controllers

import views._
import play.api._
import play.api.libs.json.JsValue
import play.api.mvc._

object Application extends Controller {

  def index = Action { implicit request => 
    Ok(html.index("BaghChal", "testUser"))
  }

   def webSocket(id:String) = WebSocket.async[JsValue] { request  =>
    Websocket.join(id)
  }
  
}