package controllers

import akka.actor._
import scala.concurrent.duration._
import scala.language.postfixOps
import play.api._
import play.api.libs.json._
import play.api.libs.iteratee._
import play.api.libs.concurrent._
import akka.util.Timeout
import akka.pattern.ask
import play.api.Play.current
import play.api.libs.concurrent.Execution.Implicits._

object Websocket {
  implicit val timeout = Timeout(1 second)
  
  lazy val default = {
    val roomActor = Akka.system.actorOf(Props[Websocket])
    
    roomActor
  }

  def join(username:String):scala.concurrent.Future[(Iteratee[JsValue,_],Enumerator[JsValue])] = {
    (default ? Join(username)).map {
      case Connected(enumerator) =>
        // Create an Iteratee to consume the feed
        val iteratee = Iteratee.foreach[JsValue] { event =>
          default ! Talk(username, (event).as[JsValue])
        }.map { _ =>
          default ! Quit(username)
        }
        (iteratee,enumerator)
      case CannotConnect(error) => 
        // Connection error
        // A finished Iteratee sending EOF
        val iteratee = Done[JsValue,Unit]((),Input.EOF)
        // Send an error and close the socket
        val enumerator = Enumerator[JsValue](JsObject(Seq("error" -> JsString(error)))).andThen(Enumerator.enumInput(Input.EOF)) 
        (iteratee,enumerator)         
    }
  }
}

class Websocket extends Actor {
  
  var members = Set.empty[String]
  val (enumerator, channel) = Concurrent.broadcast[JsValue]
  
  def channels = (enumerator, channel)
  
  def receive = {
    
    case Join(username) => {
      if(members.contains(username)) {
        sender ! CannotConnect("This username is already used")
      } else {
        members = members + username
        sender ! Connected(enumerator)
        self ! NotifyJoin(username)
      }
    }

    case NotifyJoin(username) => {
        println(username + " connected!")

        var msg = JsObject(Seq(
          "type" -> JsString("BaghChal"),
          "command" -> JsString("load"),
          "text" -> JsString("Happy Playing!"))
        ).as[JsValue]
        
        Thread.sleep(50)
        channel.push(msg)
    }
    
    case Talk(username, text) => {
        BaghChalViewController.commandHandling(text,channel)
    }
    
    case Quit(username) => {
      members = members - username
      println(username + " disconnected!")
      System.gc()
    }
    
  }
  
}

case class Join(username: String)
case class Quit(username: String)
case class Talk(username: String, text: JsValue)
case class NotifyJoin(username: String)

case class Connected(enumerator:Enumerator[JsValue])
case class CannotConnect(msg: String)