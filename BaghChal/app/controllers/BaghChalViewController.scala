package controllers

import java.io.{OutputStreamWriter, FileOutputStream, File}
import play.api._
import play.api.mvc._
import play.api.libs.json._
import play.api.libs.iteratee._
import play.api.libs.concurrent._
import play.api.libs.iteratee.Concurrent.Channel
import baghchal.controller.BaghChalController
import baghchal.controller.CellChanged
import baghchal.controller.StatusChanged
import baghchal.view.Tui
import baghchal.view.SwingGui
import baghchal.logic.util.Status
import scala.swing.Reactor

object BaghChalViewController extends Reactor{

  val controller = new BaghChalController
  val tui = new Tui(controller)

  val gui = new SwingGui(controller)
  var channel:Channel[JsValue] = null

  listenTo(controller)

  def loadError = JsObject(Seq(
      "type" -> JsString("game"),
      "command" -> JsString("error"),
      "text" -> JsString("Something went wrong while loading!"))
    ).as[JsValue];

  def commandHandling(message: JsValue, channel:Channel[JsValue]) = {

    if (channel != null)
    {
	    val messageType = (message \ "type").as[String]
	    val command = (message \ "command").as[String]
	
	    messageType match { 
	      case "game" => gameCommandHandling(message, command, channel)
	      case _ => println("Received undefined messages from websocket.")
	    }
    }

  }

  def gameCommandHandling(message: JsValue, command: String, channel:Channel[JsValue]) = {
    
    val msg = message.as[JsObject]
    this.channel = channel

    command match {
      case "new" => {
        controller.reset
      }
      case "set" => {
        val exec = (msg \ "exec").toString.replace("\"", "")
        handle(exec)
      }
      case "redraw" => {

        val row = (msg \ "row").as[String]
        var column = (msg \ "column").as[String]
        val rowInt = (msg \ "row").toString.replace("\"", "").toInt
        val columnInt = (msg \ "column").toString.replace("\"", "").toInt

        var status = "empty"
        if ( controller.game.cells(rowInt)(columnInt).status == Status.tiger ){
            status = "tiger"
        }
        if ( controller.game.cells(rowInt)(columnInt).status == Status.goat ){
            status = "goat"
        }

        val message = JsObject(Seq(
          "type" -> JsString("game"),
          "command" -> JsString("redraw"),
          "status" -> JsString(status),
          "row" -> JsString(row),
          "column" -> JsString(column))
        ).as[JsValue];
        channel.push(message)
      }
      case "inform" => {
        printStatus
      }
      case _ => channel.push(loadError)
    }
  }

  def handle(command:String) {
      tui.processInputLine(command)
  }

  def printWui = {
      val message = JsObject(Seq(
        "type" -> JsString("game"),
        "command" -> JsString("redrawInit"))
      ).as[JsValue];
      channel.push(message)
  }

  def printStatus = {
      val status = controller.statusText
      val placedGoatsCount = controller.goats.toString
      val turn = controller.game.turn.toString
      val score = controller.score.toString
      val message = JsObject(Seq(
        "type" -> JsString("game"),
        "command" -> JsString("inform"),
        "status" -> JsString(status),
        "placedGoatsCount" -> JsString(placedGoatsCount),
        "turn" -> JsString(turn),
        "score" -> JsString(score))
      ).as[JsValue];
      channel.push(message)
  }

  reactions += {
    case CellChanged => printWui
    case StatusChanged => printStatus
  }
}