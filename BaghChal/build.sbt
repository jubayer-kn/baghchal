name := "BaghChal"

version := "1.0-SNAPSHOT"

libraryDependencies ++= Seq(
  jdbc,
  anorm,
  cache
)     

libraryDependencies += "org.scala-lang" % "scala-swing" % "2.10.3"

libraryDependencies += "org.mockito" % "mockito-all" % "1.9.5"

play.Project.playScalaSettings

ScctPlugin.instrumentSettings
